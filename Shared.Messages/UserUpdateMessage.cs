﻿using System;

namespace Shared.Messages {
    public class UserUpdateMessage {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Tag { get; set; }
    }
}
